from django.shortcuts import get_object_or_404, render, redirect
from .models import News, Tips
from .forms import NewsForm, TipsForm
from django.contrib import messages

def home(request):
    news = News.objects.all()
    tips = Tips.objects.all()
    response = {'confirmed_case': '361,867', 'active_case': '64,032', 'recovered': '258,324', 'dead': '12,511', 'news': news, 'tips': tips}
    return render(request, 'main/home.html', response)

def news(request):
    form = NewsForm(request.POST or None)
    response = {'form': form}
    return render(request, 'main/news.html', response)

def tips(request):
    form = TipsForm(request.POST or None)
    response = {'form': form}
    return render(request, 'main/tips.html', response)

def add_tips(request):
    if request.method == 'POST':
        form = TipsForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Tips added successfully!")
            return redirect('main:home')
        else:
            messages.error(request, "The uploaded file must be an image!")
            return redirect('main:tips')
    form = TipsForm()
    response = {'form': form}
    return render(request, 'main/home.html', response)

def remove_tips(request, id):
    tips = get_object_or_404(Tips, pk=id)
    tips.delete()
    messages.success(request, "Tips has been deleted")
    return redirect('main:home')

def add_news(request):
    form = NewsForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            messages.success(request, "News added successfully!")
        return redirect('main:home')
    response = {'form': form}
    return render(request, 'main/home.html', response)

def remove_news(request, id):
    news = get_object_or_404(News, pk=id)
    news.delete()
    messages.success(request, "News has been deleted")
    return redirect('main:home')
