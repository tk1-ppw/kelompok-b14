from django.forms import ModelForm
from .models import News, Tips

class NewsForm(ModelForm):
    class Meta:
        model = News
        fields = '__all__'

class TipsForm(ModelForm):
    class Meta:
        model = Tips
        fields = '__all__'
