from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class UserAccount(AbstractUser):
   email = models.EmailField(unique=True, verbose_name="Email")
   covid_result = models.CharField(max_length=20, verbose_name="Covid Result", null=True)
   test_taken = models.DateTimeField(verbose_name="Time and Date of Last COVID Test", null=True)

   REQUIRED_FIELDS = ['email', 'password']

   class Meta:
      verbose_name = 'user'
      verbose_name_plural = 'users'
