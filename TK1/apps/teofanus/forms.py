from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm


class RegisterForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ['username', 'email', 'password1', 'password2']
    # def clean(self):
    #     email = self.cleaned_data.get('email')
    #     password = self.cleaned_data.get('password')

    #     if email is not None and password:
    #         self.user_cache = authenticate(self.request, email=email, password=password)
    #         if self.user_cache is None:
    #             raise self.get_invalid_login_error()
    #         else:
    #             self.confirm_login_allowed(self.user_cache)

    #     return self.cleaned_data

# class RegisterForm(forms.ModelForm):
#     class Meta:
#         model = User
#         fields = '__all__'
#         labels = {
#             'name': 'Nama'
#         }
#         widgets = {
#             'password': forms.PasswordInput()
#         }


# class LoginForm(forms.ModelForm):
#     class Meta:
#         model = User
#         exclude = ['name']
#         widgets = {
#             'password': forms.PasswordInput()
#         }
