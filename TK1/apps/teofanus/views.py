from django.shortcuts import redirect, render
from .forms import RegisterForm
from django.contrib.auth.forms import  AuthenticationForm
from django.contrib import messages
from django.contrib.auth import authenticate, logout
from django.contrib.auth import login as auth_login

# Create your views here.

def login(request):
    response = {'login_form': AuthenticationForm}
    return render(request, 'teofanus/login.html', response)

def register(request):
    response = {'register_form': RegisterForm}
    return render(request, 'teofanus/register.html', response)

def register_account(request):
    form = RegisterForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            messages.success(request, "Account created successfully!")  
            return redirect('teofanus:register')
    response = {'register_form': form}
    return render(request, 'teofanus/register.html', response)

def login_account(request):
    form = AuthenticationForm(request.POST or None)
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username = username, password = password)
        
        if user is not None:
            auth_login(request, user)
            messages.success(request, "You have logged in successfully!")
            return redirect('teofanus:login')
        else:
            messages.error(
                request, "No account can be found, please check your credentials or register first if you have not done so")
            return redirect('teofanus:login')
    response = {'login_form': form}
    return render(request, 'teofanus/login.html', response)

def logout_account(request):
    logout(request)
    return redirect('main:home')
