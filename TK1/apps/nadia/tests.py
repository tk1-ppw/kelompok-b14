from django.test import TestCase, Client
from TK1.apps.nadia.models import *
# Create your tests here.

class TestLokasiTest(TestCase):
    def test_apakah_url_lokasiTest_ada(self):
        response = self.client.get('/rapid-test/lokasiTest')
        self.assertEquals(response.status_code, 200)

    def test_apakah_di_halaman_lokasiTest_ada_templatenya(self):
        response = self.client.get('/rapid-test/lokasiTest')
        self.assertTemplateUsed(response, 'lokasiTest.html')

    def test_apakah_di_halaman_lokasiTest_ada_text(self):
        response = self.client.get('/rapid-test/lokasiTest')
        html = response.content.decode('utf8')
        self.assertIn("Masukkan lokasi anda", html)

    def test_KotaModel(self):
        KotaModel.objects.create(nama_kota='kota')
        kota = KotaModel.objects.get(nama_kota='kota')
        self.assertEqual(str(kota), 'kota')

    def test_TempatModel(self):
        kota = KotaModel.objects.create(nama_kota='kota')
        TempatModel.objects.create(nama_tempat='tempat', alamat="alamat", gmaps="test", deskripsi="deskripsi", lokasi= kota)
        tempat = TempatModel.objects.get(nama_tempat='tempat')
        self.assertEqual(str(tempat), 'tempat')

   