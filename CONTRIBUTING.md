# Collaboration Guide

## Persiapan

1. Clone reponya `git clone https://gitlab.com/tk1-ppw/kelompok-b14` dan buka VSCode (atau text editor lainnya) di folder tersebut.
2. Set remote repository, `git remote add origin https://gitlab.com/tk1-ppw/kelompok-b14`
3. Ganti ke branch kalian, `git checkout -b <nama_branch>`

    ![Branch Name][branch]

4. Siapkan virtual environment `python -m venv venv` dan aktifkan dengan `.\venv\Scripts\activate`.
5. Install requirements dari requirements.txt, `python -m pip install requirements.txt`.

## Pengerjaan

1. Di dalam folder `TK1/apps` udah disiapin aplikasi tempat pengerjaan (ada folder `static` sama `templates`)
2. Di dalam folder `static` dan `templates` memang ada folder lagi yang isinya nama kalian dan kalian taro templates dan static files di folder yang isinya nama kalian itu. Kenapa ga langsung di folder templates atau static aja? Karena ketika Django mencari sebuah resource (static atau template) dan menemukan beberapa resources yang sama, yang bakal ke load cuman 1. Jadi dibuat ada folder lagi biar walaupun namanya bentrok, tetep bakal ke load semua.

> Ketika mau pake filenya, make pathnya `<nama_kalian>/<lokasi_file>`, contoh: `{% static 'teofanus/static/css/index.css' %}`. Contoh lain:
> ```python
> def index(request):
>     return render(request, 'teofanus/index.html')
> ```

3. Ketika mengerjakan template, pastikan:
    * Mengextend base.html `{% extends 'base.html' %}`
    * Mengisi block meta dengan title

    ```html
    {% block meta %}
    <title>Login</title>
    {% endblock meta %}
    ```

    * Mengisi bagian kalian pada block content

    ```html
    {% block content %}
    <!-- bla bla bla -->
    {% endblock content %}
    ```

## Menjalankan Test

Gunakan command `python manage.py test .\TK1\apps\<nama_folder_kalian>`. Contoh: `python manage.py test .\TK1\apps\patrick`

## Push Ke GitLab

Ketika ingin push ke gitlab, pastikan kalo pushnya ke branch masing", gunakan perintah `git push -u origin <nama_branch>`. Untuk push yang kedua dan seterusnya, kalian bisa push dengan perintah `git push` saja.

[branch]: ./TK1/static/images/branch.png
